# AppImages

## Aplicativos guardados no Git

## Aplicativos não guardados no Git

Devem ser transferidos da internet, ou alguns aplicativos estão guardados noutra pastas não-git:

* **Graviton**
  - **Descrição:** Um editor de código com uma aparência mais moderna
  - **Hiperligação:** https://github.com/Graviton-Code-Editor/Graviton-App

* **Index**
  - **Descrição:** Index é um gerenciador de arquivos, que permite que você acesse seus arquivos de forma rápida e fácil.
  - **Hiperligação**: http://mauikit.org/apps/index

* **Micropad**:
  - **Descrição:** Micropad é um editor de texto simples, que permite que você escreva e edite rapidamente.
  - **Hiperligação:** https://getmicropad.com/ ou https://github.com/MicroPad/MicroPad-Electron